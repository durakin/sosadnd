import os
import uuid

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.files.storage import FileSystemStorage
from django.db import models


class OverwriteStorage(FileSystemStorage):
    def get_available_name(self, name, max_length=None):
        if self.exists(name):
            os.remove(os.path.join(settings.MEDIA_ROOT, name))
        return name


def path_to_hero_instance_filename(instance, filename=''):
    return f"list_data/{instance.user.id}/config/{instance.list_uuid}"


def path_to_allies_organizations_symbol_image(instance, filename=''):
    return f"list_data/{instance.user.id}/allies/allies_{instance.list_uuid}"


def path_to_character_appearance_image(instance, filename=''):
    return f"list_data/{instance.user.id}/appearance/appearance_{instance.list_uuid}"


class List(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, default='')
    published = models.BooleanField(default=False)
    likes = models.BigIntegerField(default=0)
    likers = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='Likers')
    downloads = models.BigIntegerField(default=0)
    downloaders = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='Downloaders')

    hero_configuration_list = models.FileField(upload_to=path_to_hero_instance_filename, storage=OverwriteStorage(),
                                               null=True)
    allies_organizations_symbol = models.ImageField(upload_to=path_to_allies_organizations_symbol_image,
                                                    storage=OverwriteStorage(), blank=True, null=True)
    character_appearance = models.ImageField(upload_to=path_to_character_appearance_image, storage=OverwriteStorage(),
                                             blank=True, null=True)

    list_uuid = models.UUIDField(unique=True, default=uuid.uuid4, blank=False)

    def update_hero_configuration_list(self, list_config):
        list_config_json_file = ContentFile(list_config)
        self.hero_configuration_list.save(f'{self.user.username}_{self.list_uuid}', list_config_json_file, save=True)

    @property
    def json_conf(self):
        with open(self.hero_configuration_list.path, "r") as config:
            return config.read()

    def publicate(self):
        self.published = True
        self.save()

    def refuse(self):
        self.published = False
        self.save()

    def download(self, user):
        if not (user in self.downloaders.all()):
            self.downloads += 1
            self.downloaders.add(user)
        else:
            self.downloaders.add(user)
        self.save()

    def toggle_like(self, user):
        if user in self.likers.all():
            self.likers.remove(user)
            self.likes -= 1
            self.save()
            return False
        else:
            self.likers.add(user)
            self.likes += 1
            self.save()
            return True

    def duplicate_to(self, new_user):
        new_list = List(user=new_user, list_uuid=uuid.uuid4())
        new_list.update_hero_configuration_list(self.json_conf)
        if self.character_appearance:
            character_appearance_copy = ContentFile(self.character_appearance.read())
            new_list.character_appearance.save(path_to_character_appearance_image(new_list), character_appearance_copy)
        if self.allies_organizations_symbol:
            allies_organizations_symbol_copy = ContentFile(self.allies_organizations_symbol.read())
            new_list.allies_organizations_symbol.save(path_to_allies_organizations_symbol_image(new_list), allies_organizations_symbol_copy)
        new_list.save()
        return new_list


    def delete(self, using=None, keep_parents=False):
        if self.allies_organizations_symbol:
            self.allies_organizations_symbol.delete()
        if self.character_appearance:
            self.character_appearance.delete()
        super().delete()

    def __repr__(self):
        return f"List {self.list_uuid}"

    def __str__(self):
        return f"Owner: {self.user.username}, Published: {self.published}, Likes: {self.likes}," \
               f"Downloads: {self.downloads}\n"

    class Meta:
        ordering = ['user', 'published']
