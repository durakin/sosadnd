# Generated by Django 3.0.5 on 2020-07-07 09:44

from django.conf import settings
from django.db import migrations, models
import manage_lists.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('manage_lists', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='list',
            options={'ordering': ['user', 'published']},
        ),
        migrations.AddField(
            model_name='list',
            name='_hash',
            field=models.CharField(default='', help_text='List instance hash', max_length=32),
        ),
        migrations.AddField(
            model_name='list',
            name='allies_organizations_symbol',
            field=models.ImageField(null=True, upload_to=manage_lists.models.path_to_allies_organizations_symbol_image),
        ),
        migrations.AddField(
            model_name='list',
            name='character_appearance',
            field=models.ImageField(null=True, upload_to=manage_lists.models.path_to_character_appearance_image),
        ),
        migrations.AddField(
            model_name='list',
            name='downloaders',
            field=models.ManyToManyField(related_name='Downloaders', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='list',
            name='downloads',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AddField(
            model_name='list',
            name='hero_configuration_list',
            field=models.FileField(default='', upload_to=manage_lists.models.path_to_hero_instance_filename),
        ),
        migrations.AddField(
            model_name='list',
            name='likers',
            field=models.ManyToManyField(related_name='Likers', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='list',
            name='likes',
            field=models.BigIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='list',
            name='published',
            field=models.BooleanField(default=False),
        ),
    ]
