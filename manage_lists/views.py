import json
import uuid

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.http import FileResponse, Http404, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render, reverse
from pdfrw import PdfReader, PdfWriter

from .models import List

PATH_TO_PDF_TEMPLATE: str = f"{settings.BASE_DIR}/manage_lists/static/manage_lists/5E_CharacterSheet_Fillable_id.pdf"
PATH_TO_PDF_SPACE_TEMPLATE = f"{settings.BASE_DIR}/manage_lists/static/manage_lists/5E_CharacterSheet_Fillable_space.pdf"


def get_pdf(request, list_uuid):
    """
    Function returns pdf file.
    :param list_uuid: list_uuid
    :param request: Http request (ajax POST)
    :return: pdf
    """
    if not list_uuid:
        raise Http404
    try:
        local_list = get_object_or_404(List, list_uuid=list_uuid)
        local_list_json_config = json.loads(local_list.json_conf)
        pdf_file = PdfReader(PATH_TO_PDF_TEMPLATE)

        accomp_dict = {}
        for field_id in local_list_json_config.keys():
            accomp_dict[f"{field_id}"] = []

        for (index, item) in enumerate(pdf_file.Root.AcroForm.Fields):
            pdf_field_val = f"{item.V}".strip('() ')
            if pdf_field_val in accomp_dict.keys():
                accomp_dict[pdf_field_val].append(index)

        pdf_editable = set_fields_values(PdfReader(PATH_TO_PDF_SPACE_TEMPLATE), local_list_json_config, accomp_dict)

        path_to_created_pdf = f"{settings.BASE_DIR}/media/pdf/{request.user.id}_{list_uuid}.pdf"
        PdfWriter().write(path_to_created_pdf, pdf_editable)
        return FileResponse(open(path_to_created_pdf, 'rb'), filename="dnd-hero.pdf")
    except:
        return HttpResponse("<h1>Получение пдф на этом устройстве не поддерживается."
                            "Воспользуйтесь другим устройством или попробуйте позже.</h1>")


def set_fields_values(pdf, values_obj, accomp_dict):
    """
    Functions updates fields in pdf file.
    :param accomp_dict:
    :param pdf: pdf file
    :param values_obj: python dict
    :return: pdf file
    """
    for key in accomp_dict.keys():
        pdf = update_pdf_field(pdf, accomp_dict[key], values_obj[key])
    return pdf


def update_pdf_field(pdf, field_indexes, value):
    """
    Function changes field value in pdf.
    :param pdf: Pdf object.
    :param field_indexes: list of field indexes
    :param value: New value
    :return: pdf object
    """
    for field_number in field_indexes:
        try:
            pdf.Root.AcroForm.Fields[int(field_number)].AP.N.stream = '''/Tx BMC
            BT
             /Helvetica 8.0 Tf
             1.0 5.0 Td
             0 g
             (''' + value + ''') Tj
            ET EMC'''
        except ValueError or TypeError:
            return pdf
    return pdf


@login_required
def delete_global_hero(request):
    if not request.user.is_authenticated:
        return redirect(reverse('main_man:index_page'))

    list_uuid = request.POST.get("list_uuid", None)
    if not list_uuid:
        raise Http404

    global_list_refused = get_object_or_404(List, user=request.user, list_uuid=list_uuid)
    global_list_refused.refuse()
    return JsonResponse({"OK": True})


@login_required
def delete_local_hero(request):
    if not request.user.is_authenticated:
        return redirect(reverse('main_man:index_page'))

    list_uuid = request.POST.get("list_uuid", None)
    if not list_uuid:
        raise Http404

    deleted_local_list = get_object_or_404(List, user=request.user, list_uuid=list_uuid)
    deleted_local_list.hero_configuration_list.delete()
    deleted_local_list.delete()
    return JsonResponse({"OK": True})


def create_hero(request):
    """
    Function returns creating hero page.
    :param request: Http request
    :return: HttpResponse object by method 'render' or 404 in case of errors.
    """
    return render(request, "manage_lists/list_creator.html", {"request": request})


def add_hero(request):
    """
    Function adds global list to user's private list-collection.
    :param request: Http request
    :return: JsonResponse object
    """
    if not request.user.is_authenticated:
        return JsonResponse({"url": reverse('manage_lists:locale_collection')})

    list_uuid = request.POST.get("list_uuid", None)
    if not list_uuid:
        raise Http404

    global_list__copied = get_object_or_404(List, list_uuid=list_uuid)
    global_list__copied.download(request.user)
    global_list__copied.duplicate_to(request.user)
    return JsonResponse(
        {
            "upd_downloads": global_list__copied.downloads,
            "url": reverse('manage_lists:locale_collection'),
            "updated_downloads": global_list__copied.downloads,
        }
    )


def like_hero(request):
    """
    Function adds new like to published list or reset existing like.
    :param request: Http request
    :return: JsonResponse object
    """
    if not request.user.is_authenticated:
        return JsonResponse({"code": "not allowed"})

    list_uuid = request.POST.get("list_uuid", None)
    if not list_uuid:
        raise Http404
    global_list = get_object_or_404(List, list_uuid=list_uuid)
    was_like_added = global_list.toggle_like(request.user)
    return JsonResponse({
        "upd_likes": global_list.likes,
        "was_like_added": was_like_added,
        "updated_likes": global_list.likes,
    })


def see_global_collection(request):
    """
    Function returns page with all lists added in global collection.
    :param request: Http request
    :return: HttpResponse object by render method
    """
    # TODO: add page pagination
    global_list_collection = List.objects.filter(published=True)
    for global_list in global_list_collection:
        object_configuration = json.loads(global_list.json_conf)
        global_list.player_name = object_configuration["PLAYER_NAME"]
        global_list.character_name = object_configuration["CHARACTER_NAME"]
        global_list.character_race = object_configuration["RACE"]
        global_list.backstory = object_configuration["CHARACTER_BACKSTORY"]
        global_list.likeActionClass = '_enable' if (request.user in global_list.likers.all()) else '_disable'

    return render(request, "manage_lists/global_lists.html", {"heroes": global_list_collection, "seeing_global": True})


@login_required
def publish_hero(request):
    """
    Function adds private list to global collection.
    :param request: Http request
    :return: JsonResponse object
    """
    list_uuid = request.POST.get("list_uuid", None)
    if not list_uuid:
        raise Http404

    publicated_list = get_object_or_404(List, user=request.user, list_uuid=list_uuid)
    publicated_list.publicate()
    return JsonResponse({"url": reverse('manage_lists:collection')})


def edit_hero(request, list_uuid):
    """
    Function edits existing hero list.
    :param request: Ajax request (get)
    :param list_uuid: value of uuid.uuid4()
    :return: Redirect on edit-list page
    """
    updating_user_list = get_object_or_404(List, list_uuid=list_uuid)
    context = {"updating_user_list": updating_user_list, "is_mine": request.user == updating_user_list.user}
    return render(request, "manage_lists/list_creator.html", context=context)


@login_required
def see_locale_collection(request):
    """
    Function returns page with user's heroes.
    :param request: Http request
    :return: HttpResponse object by method 'render' or 404 in case of errors.
    """
    user_list_collection = List.objects.filter(user=request.user)

    for user_list in user_list_collection:
        object_configuration = json.loads(user_list.json_conf)
        user_list.character_name = object_configuration["CHARACTER_NAME"]
        user_list.character_race = object_configuration["RACE"]
        user_list.backstory = object_configuration["CHARACTER_BACKSTORY"]
    return render(request, "manage_lists/local_lists.html", {"heroes": user_list_collection, "seeing_local": True})


def save_created_hero(request):
    """
    Function saves new hero list.
    :param request: Http request
    :return: JsonResponse object
    """
    if not request.user.is_authenticated:
        return redirect(reverse('main_man:index_page'))

    list_config = request.POST.get("list_config", None)
    if not list_config:
        raise Http404

    list_uuid = request.POST.get("list_uuid", None)
    if list_uuid:
        edited_list = get_object_or_404(List, user=request.user, list_uuid=list_uuid)
    else:
        edited_list = List(user=request.user)
        edited_list.list_uuid = uuid.uuid4()

    # saving images
    character_appearance_img = request.FILES.get("character_appearance", None)
    if character_appearance_img:
        edited_list.character_appearance = character_appearance_img

    allies_organizations_symbol_img = request.FILES.get("allies_organizations_symbol", None)
    if allies_organizations_symbol_img:
        edited_list.allies_organizations_symbol = allies_organizations_symbol_img

    # TODO: delete image if user want
    edited_list.update_hero_configuration_list(list_config)
    return JsonResponse({"redirect_after_list_updating_url": reverse('manage_lists:locale_collection')})
