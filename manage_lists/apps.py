from django.apps import AppConfig


class ManageListsConfig(AppConfig):
    name = 'manage_lists'
