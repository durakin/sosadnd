from unittest import TestCase
from manage_dices import models


class GetIntegerTests(TestCase):
    """
    Testing view function: get_integer(input_str)
    Function returns converted number or 0 if error arises.
    """

    def test_with_str_consists_of_numbers_0(self):
        self.assertEqual(1234, models.str_to_int('01234'))

    def test_with_str_consists_of_numbers_1(self):
        self.assertEqual(8348, models.str_to_int('8348'))

    def test_with_str_consists_of_numbers_2(self):
        self.assertEqual(-498, models.str_to_int('-498'))

    def test_with_str_consists_of_numbers_3(self):
        self.assertEqual(0, models.str_to_int('000000'))

    def test_with_empty_str(self):
        self.assertEqual(0, models.str_to_int(''))

    def test_with_str_0(self):
        self.assertEqual(0, models.str_to_int('abc'))

    def test_with_str_1(self):
        self.assertEqual(0, models.str_to_int('123abc'))

    def test_without_any_args(self):
        self.assertEqual(0, models.str_to_int())
