from django.contrib.auth.decorators import login_required
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import auth
from django.contrib.auth.views import LoginView
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from users.models import CustomUser
from .forms import LoginForm, RegisterForm, AccountSettingsForm


class ResetPasswordPageView(generic.TemplateView):
    template_name = "main_man/reset_password.html"


class DeleteUserView(generic.DeleteView):
    model = CustomUser
    success_url = '/'

    def get_object(self, queryset=None):
        try:
            return CustomUser.objects.get(pk=self.request.user.pk)
        except CustomUser.DoesNotExist:
            raise PermissionError('This user does not exist or you have not permissions')


class SettingsView(generic.TemplateView):
    template_name = "main_man/account_settings.html"

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        prev_data = {
            "username": self.request.user.username,
            "email": self.request.user.email,
        }
        context['account_settings_form'] = AccountSettingsForm(self.request.POST, self.request.user, initial=prev_data)
        return context

    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return redirect(reverse('main_man:index_page'))
        return render(request, self.template_name, context=self.get_context_data())


class UpdateAccountView(generic.View):
    def post(self, request):
        account_settings_form = AccountSettingsForm(request.POST, user=request.user)
        if account_settings_form.is_valid():
            username = account_settings_form.cleaned_data['username']
            email = account_settings_form.cleaned_data['email']
            psw_1 = account_settings_form.cleaned_data['psw_1']

            me = get_object_or_404(CustomUser, id=request.user.pk)
            me.username = username if username else me.username
            me.email = email if email else me.email
            if psw_1:
                me.password = make_password(psw_1)
            me.save()
            return redirect(reverse('main_man:account_page'))
        else:
            print("Error")
            print(account_settings_form.errors.as_json())
            return render(request, SettingsView.template_name, context={
                "account_settings_form": account_settings_form,
            })


class RegisterView(generic.View):
    def post(self, request):
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            username = register_form.cleaned_data['new_username']
            psw1 = register_form.cleaned_data['psw_1']
            encrypt_password = make_password(psw1)
            CustomUser(username=username, password=encrypt_password).save()
            user = auth.authenticate(username=username, password=psw1)
            auth.login(request, user)
            return redirect(reverse('main_man:account_page'))
        else:
            return render(request, 'main_man/index.html', context={
                "login_form": LoginForm(self.request.POST),
                "register_form": register_form,
            })


class MyLoginView(LoginView):
    def post(self, request, *args, **kwargs):
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            name = login_form.cleaned_data['username']
            password = login_form.cleaned_data['password']
            user = auth.authenticate(username=name, password=password)
            if user is not None:
                if user.is_active:
                    auth.login(request, user)
                    return redirect(reverse('main_man:account_page'))
        return redirect(reverse('main_man:index_page'))


@method_decorator(login_required, name='dispatch')
class AccountView(generic.TemplateView):
    template_name = 'main_man/main_account_page.html'


class IndexView(generic.TemplateView):
    template_name = 'main_man/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['login_form'] = LoginForm()
        context['register_form'] = RegisterForm()
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect(reverse('main_man:account_page'))
        return render(request, self.template_name, context=self.get_context_data())
